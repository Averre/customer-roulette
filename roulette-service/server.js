const dotenv = require("dotenv");
dotenv.config();

const cron = require("node-cron");

const mongoose = require("mongoose");
const mongoDB =
  "mongodb://" +
  process.env.DB_URL +
  ":" +
  process.env.DB_PORT +
  "/" +
  process.env.DB_NAME;
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));

const giftofferService = require("./app/services/giftoffer_service");

cron.schedule("0 * * * *", async () => {
  console.log("Started selecting a customer for a gift offer.");
  giftofferService.produceGiftOffer();
});
