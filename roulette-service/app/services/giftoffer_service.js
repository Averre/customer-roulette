const Customer = require("../models/customer");
const Giftoffer = require("../models/giftoffer");

exports.produceGiftOffer = async () => {
  const customers = await Customer.aggregate().sample(1);

  if (customers.length) {
    saveGiftOffer(customers[0]);
  } else {
    console.error("Failed in retrieval of random customer");
  }
};

async function saveGiftOffer(customer) {
  const giftoffer = new Giftoffer({
    customerNumber: customer.customerNumber,
    email: customer.email,
    redeemed: false,
    offerDate: new Date(new Date().toUTCString()),
  });

  giftoffer
    .save(giftoffer)
    .then((data) => {
      console.log(
        "Customer " + data.customerNumber + " selected for gift offer."
      );

      // TODO
      // Either send email with offer or MQ message with customer number to be consumed by other responsible services.
    })
    .catch((err) => {
      console.error("Error when persisting new gift offer for customer " + customer.customerNumber);
      console.error("Error: " + err);
    });
}
