const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const GiftofferModelSchema = new Schema({
  customerNumber: { type: Number, required: true },
  email: { type: String, required: true },
  redeemed: { type: Boolean, required: true },
  offerDate: {type: Date, required:true}
});

module.exports = mongoose.model("GiftofferModel", GiftofferModelSchema);
