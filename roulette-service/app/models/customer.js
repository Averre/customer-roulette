const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const CustomerModelSchema = new Schema(
  {
    customerNumber: { type: Number, required: true },
    email: { type: String, required: true },
  }
);

module.exports = mongoose.model("CustomerModel", CustomerModelSchema);
