const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const CustomerModelSchema = new Schema(
  {
    customerNumber: { type: Number, required: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, required: true },
  },
  { timestamps: true }
);

module.exports = mongoose.model("CustomerModel", CustomerModelSchema);
