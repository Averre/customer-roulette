const Customer = require("../models/customer");

exports.add = (data, callback) => {
  const customer = new Customer({
    customerNumber: data.customerNumber,
    firstName: data.firstName,
    lastName: data.lastName,
    email: data.email,
  });

  customer
    .save(customer)
    .then((data) => {
      callback(null, data._id);
    })
    .catch((err) => {
      console.log("Error when persisting new Customer " + err);
      callback(err, null);
    });
};

exports.findById = (id, callback) => {
  Customer.findById(id, (err, customer) => {
    callback(err, customer);
  });
}
