const customerService = require("../services/customer_service");

module.exports = function (app) {

  // Add Customer
  app.post("/customer", (req, res) => {
    customerService.add(req.body, (err, id) => {
      if (err) {
        console.log("Attemp to add a customer failed with " + err);
        res.setStatus(500).send("Failed in adding a new customer");
      } else {
        res.send({ _id: id });
      }
    });
  });

  // Get Customer
  app.get("/customer/:id", (req, res) => {
    customerService.findById(req.params.id, (err, customer) => {
      if (err) {
        console.log("Failed to retrieve customer with id " + req.params.id);
        console.log("Error " + err);
        res.setStatus(500);
      } else {
        if (!customer) {
          res.status(400).send("No customer found with _id: " + req.params.id);
        }
  
        res.send(customer);
      }
    });
  });

  // Update Customer
  app.put("/customer/:id", (req, res) => {
    // TODO
    // Update the customer in customers database
    // Send MQ message with customer number status UPDATED
    
    res.sendStatus(501);
  });

  // Delete Customer
  app.delete("/customer/:id", (req, res) => {
    // TODO
    // Delete customer from customers database
    // Send MQ message with customer number and status DELETED
    
    res.sendStatus(501);
  });
};
