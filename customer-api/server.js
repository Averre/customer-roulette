const dotenv = require("dotenv");
dotenv.config();

const mongoose = require("mongoose");
const mongoDB =
  "mongodb://" +
  process.env.DB_URL +
  ":" +
  process.env.DB_PORT +
  "/" +
  process.env.DB_NAME;
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));

const express = require("express");

const app = express();
const port = process.env.PORT;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

require("./app/routes")(app);

app.listen(port, () => {
  console.log("Started Customers API Service");
});
