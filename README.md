# Customer Roulette #

This project contains two related Node.js services.

## customer-api ##

Simple API with endpoints to managing customers.

### Run ###
At the root of the subproject run: 

    npm run dev

The service will be available at http://localhost:3000 by default.


## roulette-service ##

Service that executes periodically with the help of node-cron.
Picks out a random customer to be offered a gift.

### Run ###
At the root of the subproject run: 

    npm run dev

The services runs every 60 minutes by default.
